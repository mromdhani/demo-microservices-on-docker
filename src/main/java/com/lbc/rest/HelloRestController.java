package com.lbc.rest;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloRestController {
	
	@GetMapping
	public List<String> sayHello() {
		return Arrays.asList("Hello", "Asslama", "Bonjour");
	}

}
