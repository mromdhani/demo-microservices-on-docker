package com.lbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMicroservicesOnDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoMicroservicesOnDockerApplication.class, args);
	}

}

